Scriptname AAAMPChest extends ObjectReference  

function Open()
	If PlayGamebryoAnimation("Open", abStartOver = True)
		Utility.Wait(0.5)
	EndIf
endFunction

function Close()
	If PlayGamebryoAnimation("Close", abStartOver = True)
		Utility.Wait(0.5)
	EndIf
endFunction