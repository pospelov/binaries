local listeners = {}
local menuError
local files = ls()
for i = 1, #files do
  if string.find(files[i], '.lua') and not string.find(files[i], 'main.lua') then
    local t = dofile(files[i])
    print('dofile' .. files[i])
    if type(t) == 'table' then
      table.insert(listeners, t)
    end
    if string.find(files[i], 'menuError.lua') then menuError = t end
  end
end

sound = {
  UIMagicSelect = 0x6be1f,
  UIMagicUnselect = 0x6be20,
  UIMapRollover = 0x3de31,
  UIMapRolloverFlyout = 0xc0df3,
  UISelectOff = 0xc8c6e,
  UISelectOn = 0xc8c6d,
  UIFavorite = 0x6b460,
  UIActivateFail = 0x6d1c6
}

function onRenderImpl()
  for i = 1, #listeners do
    local ok, err = pcall(function () emitEvent(listeners[i], 'render', {}) end)
    if not ok then menuError.setError(err) end
  end
end

function emitEvent(listener, event, eventArgs)
  if listener.on and listener.on[event] then
    listener.on[event](eventArgs[1], eventArgs[2], eventArgs[3], eventArgs[4], eventArgs[5], eventArgs[6], eventArgs[7], eventArgs[8])
  end
end

function onEvent(event, eventArgs)
  if event ~= nil then
    for i = 1, #listeners do emitEvent(listeners[i], event, eventArgs) end
  end
end

-- Вызывается каждый кадр из клиента SkyMP:
function onRender()
  onRenderImpl()

  if not g_nextMsg then
    g_nextMsg = true
    skymp.SendScriptEvent('nextmsg', '', function (code, body)
      g_nextMsg = false
      if code == 200 then
        local t = {}
        local from = ''
        local header, content
        for i = 1, #body do
          if content then
            content = content .. body[i]
          elseif header then
            if body[i] == ' ' then content = '' else header = header .. body[i] end
          else
            if body[i] == ' ' then header = '' else from = from .. body[i] end
          end
        end
        table.insert(t, from)
        table.insert(t, header)
        table.insert(t, content)
        onEvent('message', t)
      end
    end)
  end
end
