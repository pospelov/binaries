local menuExchange = {}

local CONTENT_EXCHANGE_START_OFFER = 100
local CONTENT_EXCHANGE_START_CONFIRM = 101
local CONTENT_EXCHANGE_FINISH = 103
local CONTENT_EXCHANGE_FINISH_ERROR = 104

menuExchange.reset = function ()
  menuExchange.broadcastRate = 120
  menuExchange.isDialogVisible = false
  menuExchange.isVisible = false
  menuExchange.winWidth = 900
  menuExchange.winHeightFull = 550
  menuExchange.winHeight = 500 -- Body height
  menuExchange.childHeight = menuExchange.winHeight - 16
  menuExchange.leftWidthPercentage = 0.45
  menuExchange.lockSize = { width = 32, height = nil } -- при nil высота будет как ширина
  menuExchange.lockSizeMax = { width = 42, height = nil }
  menuExchange.getLeftCaption = function () return 'Мой инвентарь' end
  menuExchange.getRightCaption = function () return tostring(menuExchange.partnerName) end
  menuExchange.getRightCaption2 = function () return tostring(menuExchange.myName) end
  menuExchange.gap = 8
  menuExchange.captionsOffsetY = menuExchange.gap
  menuExchange.myInventory = {}
  menuExchange.partnerExchangeInv = {}
  menuExchange.myExchangeInv = {}
  menuExchange.partnerLock = 'unlocked'
  menuExchange.myLock = 'unlocked'
  menuExchange.page = 'exchange'
  menuExchange.sliderMin = 1
  menuExchange.sliderMax = 10
  menuExchange.sliderVal = 1
  menuExchange.sliderId = nil
  menuExchange.sliderCaption = ''
  menuExchange.confirmButtonText = 'Подтвердить'
  menuExchange.soundOk = 0x3c751
  menuExchange.buttonActive = true
  ClearSkympDialog()
  if menuExchange.encodeInv and skymp.SendScriptEvent then
    local encoded = menuExchange.encodeInv(menuExchange.myLock, menuExchange.myName, menuExchange.myExchangeInv)
    skymp.SendScriptEvent('setvar', 'exchangeVisuals=' .. encoded, function() end)
  end
end
menuExchange.reset()

menuExchange.ids = {
  rightCaption = '###rightCaption',
  rightCaption2 = '###rightCaption2'
}

function menuExchange.encodeExchangeData()
  local str = menuExchange.myName .. ' ' .. menuExchange.partnerName .. ' ' .. menuExchange.myLock .. ' ' .. menuExchange.partnerLock  .. ' '
  local inv = menuExchange.myExchangeInv
  for i = 1, #inv do
    str = str .. skymp.GetFormInfo(inv[i].id) .. '=' .. math.floor(tonumber(inv[i].count)) .. ' '
  end
  str = str .. 'END_MY_INV' .. ' '
  inv = menuExchange.partnerExchangeInv
  for i = 1, #inv do
    str = str .. skymp.GetFormInfo(inv[i].id) .. '=' .. math.floor(tonumber(inv[i].count)) .. ' '
  end
  return str
end

function menuExchange.encodeInv(lock, name, inv)
  local str = ''
  for i = 1, #inv do
    str = str .. inv[i].id .. ' ' .. math.floor(tonumber(inv[i].count)) .. ' '
  end
  return lock .. ' ' .. name .. ' ' .. str
end

function menuExchange.decodeInv(str)
  -- https://stackoverflow.com/questions/1426954/split-string-in-lua
  local isId = true
  local slot = nil
  local res = {}
  local from = nil
  local lock = nil
  for token in string.gmatch(str, "[^%s]+") do
    if not lock then
      lock = token
    elseif not from then
      from = token
    else
      if isId then
        slot = {}
        slot.id = tonumber(token)
      else
        slot.count = tonumber(token)
        slot.name = skymp.GetFormName(slot.id)
        table.insert(res, slot)
      end
      isId = not isId
    end
  end
  return res, from, lock
end

function menuExchange.split(content)
  local s1 = ''
  local s2 = nil
  for i = 1, string.len(content) do
    if content[i] == ':' then s2 = ''
    elseif s2 == nil then s1 = s1 .. content[i]
    else s2 = s2 .. content[i] end
  end
  return s1, s2
end

menuExchange.on = {
  interactionClick = function (targetName, index)
    if tonumber(index) == 1 then
      sendto(targetName, 'CONTENT_EXCHANGE_START_OFFER', targetName .. ':' .. skymp.GetMyName())

      menuExchange.partnerName = targetName
      menuExchange.myName = skymp.GetMyName()
    end
  end,
  message = function (from, header, content)
    local myName = skymp.GetMyName()
    if header == 'CONTENT_EXCHANGE_START_OFFER' then
      menuExchange.reset()
      local targetName, senderName = menuExchange.split(content)
      if myName == targetName then
        menuExchange.isDialogVisible = true
        menuExchange.partnerName = senderName
        menuExchange.myName = myName
      end
    elseif header == 'CONTENT_EXCHANGE_START_CONFIRM' then
      local targetName, senderName = menuExchange.split(content)
      if senderName == menuExchange.partnerName and targetName == menuExchange.myName then
        menuExchange.show()
      end
    elseif header == 'CONTENT_EXCHANGE_FINISH' then
      if content == menuExchange.myName then menuExchange.finish() end
    elseif header == 'CONTENT_EXCHANGE_FINISH_ERROR' then
      if content == menuExchange.myName then menuExchange.finishError() end
    end
  end,
  broadcastIn = function (contentType, content)
    contentType = tonumber(contentType)
  end
}

function menuExchange.finish()
  skymp.LoadMyInventory()
  menuExchange.reset()
  imgui.PlaySound(sound.UIMapRollover)
  skymp.PrintNote('Обмен завершён успешно')
  imgui.SetCursorVisible(false)
end

function menuExchange.finishError()
  skymp.LoadMyInventory()
  menuExchange.reset()
  imgui.PlaySound(sound.UIActivateFail)
  skymp.PrintNote('При обмене произошла ошибка')
  imgui.SetCursorVisible(false)
end

function menuExchange.broadcastMyExchangeInv()
  menuExchange.loadPartnerVisuals()
  if not menuExchange.frame then menuExchange.frame = menuExchange.broadcastRate end
  menuExchange.frame = menuExchange.frame - 1
  if menuExchange.frame >= 0 then return end

  if not menuExchange.setVar then
    menuExchange.setVar = true
    local encoded = menuExchange.encodeInv(menuExchange.myLock, menuExchange.myName, menuExchange.myExchangeInv)
    skymp.SendScriptEvent('setvar', 'exchangeVisuals=' .. encoded, function () menuExchange.setVar = false end)
    menuExchange.frame = menuExchange.broadcastRate
  end
end

function menuExchange.loadPartnerVisuals() -- надо вызывать из render
  if menuExchange.getVarInProgress then return end
  menuExchange.getVarInProgress = true
  skymp.SendScriptEvent('getvar', menuExchange.partnerName .. '.exchangeVisuals', function (code, body)
    local success, err = pcall(function()
      menuExchange.getVarInProgress = false
      local ok, err = pcall(function ()
        local inv, from, lock = menuExchange.decodeInv(body)
        if from == menuExchange.partnerName then
          if menuExchange.partnerLock == 'unlocked' then menuExchange.partnerExchangeInv = inv end
          if lock == 'locked' then menuExchange.partnerLock = lock end
        end
      end)
      if not ok then print('loadPartnerVisuals ' .. err) end
    end)
    if not success then print(err) end
  end)
end


function menuExchange.addSlot(inv, slot)
  menuExchange.frame = 0 -- Форсируем отправку пакета с изменениями инвентаря
  for i = 1, #inv do
    if inv[i].id == slot.id then inv[i].count = inv[i].count + slot.count; return end
  end
  table.insert(inv, { id = slot.id, count = slot.count, name = slot.name })
end

function menuExchange.removeSlot(inv, slot)
  menuExchange.frame = 0 -- Форсируем отправку пакета с изменениями инвентаря
  for i = 1, #inv do
    if inv[i].id == slot.id then
      if inv[i].count > slot.count then
        inv[i].count = inv[i].count - slot.count
        return true
      elseif inv[i].count == slot.count then
        table.remove(inv, i)
        return true
      else
        return false
      end
    end
  end
  return false
end

function menuExchange.processInventoryClick(inv, clickedIndex)
  local slot = {
    id = inv[clickedIndex].id,
    count = inv[clickedIndex].count,
    name = inv[clickedIndex].name
  }
  local sliderN = 6
  if slot.count < sliderN and slot.count > 0 then slot.count = 1 end
  local soundFail = function () imgui.PlaySound(0x6d1c6) end
  local soundOk = function () imgui.PlaySound(0x6b460) end

  local oppositeInv = menuExchange.myInventory
  if inv == oppositeInv then oppositeInv = menuExchange.myExchangeInv end

  if inv == menuExchange.partnerExchangeInv then
    soundFail()
  elseif inv == menuExchange.myExchangeInv or menuExchange.myInventory then
    if menuExchange.myLock == 'locked' then return soundFail() end
    soundOk()
    if slot.count >= sliderN and inv == menuExchange.myInventory then
      menuExchange.page = 'slider'
      menuExchange.sliderCaption = slot.name
      menuExchange.sliderMin = 1
      menuExchange.sliderVal = 1
      menuExchange.sliderMax = slot.count
      menuExchange.sliderId = slot.id
    else
      menuExchange.page = 'exchange'
      menuExchange.addSlot(oppositeInv, slot)
      menuExchange.removeSlot(inv, slot)
    end
  end
end

function menuExchange.drawCaption(id, caption, width, lock)
  local clicked = false

  local textW, textH = imgui.CalcTextSize(caption)
  local cursX, cursY = imgui.GetCursorPos();

  -- Рисование замочка
  if lock then
    local path = 'lock_' .. tostring(lock) .. '.png'
    local spriteButtonId = tostring(id) -- id переданный в функцию и реальный id спрайт баттона ДОЛЖНЫ совпадать
    local w, h, maxW, maxH = menuExchange.lockSize.width, menuExchange.lockSize.height, menuExchange.lockSizeMax.width, menuExchange.lockSizeMax.height
    if not h then h = w end; if not maxH then maxH = maxW end
    imgui.SetCursorPosY(cursY + menuExchange.gap)
    if SpriteButton(spriteButtonId, path, w, h, maxW, maxH) then
      clicked = true
      menuExchange.frame = 0 -- Форсируем отправку пакета с изменениями инвентаря
    end
    imgui.SameLine()
    local cursX, cursY = imgui.GetCursorPos();
    if lock then imgui.SetCursorPosY(cursY + 0.5 * (maxH - textH)) end -- Центрирование текста по высоте напротив замочка
  else
    imgui.SetCursorPosX(0.5 * (width - textW))
  end

  if not lock then
    imgui.Text(caption)
  else
    imgui.PushStyleColor(imgui.constant.Col.Text, 0xffffcc33)
    imgui.Text(caption);
    imgui.PopStyleColor()
    imgui.SameLine();
    imgui.Text('предлагает:')
  end

  return clicked
end

function menuExchange.inBorder(childId, f, addWidth, addHeight, lockState)
  if addWidth == nil then addWidth = 0 end
  if addHeight == nil then addHeight = 0 end
  local color = 0xaaffffff
  if lockState == 'locked' then color = 0xaaffcc33 end
  imgui.PushStyleColor(imgui.constant.Col.Border, color)
  local cursX, cursY = imgui.GetCursorPos()
  cursX = cursX + menuExchange.gap
  cursY = cursY + menuExchange.gap
  imgui.SetCursorPosX(cursX)
  imgui.SetCursorPosY(cursY)
  local w, h = -menuExchange.gap + addWidth, -menuExchange.gap + addHeight
  imgui.BeginChild(childId, w, h, true)
  local success, err = pcall(f)
  imgui.EndChild()
  imgui.PopStyleColor()
  if not success then error(err) end
end

function menuExchange.getPathByType(type)
  --imgui.Text(type)
  if type ~= 32 and type ~= 41 and type ~= 26 and type ~= 46 then
    type = 32
  end
  return 'type' .. type .. '.png'
end

function menuExchange.drawInventory(inv)
  local clickedIndex = nil
  for i = 1, #(inv) do
    if inv[i].name then
      local txt = tostring(inv[i].name)
      local count = inv[i].count
      if count ~= 1 then txt = txt .. ' (' .. math.floor(tostring(count)) .. ')' end
      local col = nil
      if not imgui.IsCursorVisible() then col = -1 end
      --txt = skymp.GetFormType(inv[i].id) .. ' ' .. txt
      local type = skymp.GetFormType(inv[i].id)
      local pngPath = menuExchange.getPathByType(type)
      imgui.Image(pngPath, 32, 32)
      imgui.SameLine()
      local clicked = SkympLink('###menuExchange_skympLink' .. tostring(inv) .. tostring(i), txt, col, col)
      if clicked and imgui.IsCursorVisible() then clickedIndex = i end
    end
  end
  if clickedIndex then menuExchange.processInventoryClick(inv, clickedIndex) end
end

function menuExchange.drawLeft()
  local leftWidth = menuExchange.leftWidthPercentage * menuExchange.winWidth
  local w, h = leftWidth, menuExchange.childHeight
  imgui.BeginChild('###exchangeMenu_left', w, h, true)
  menuExchange.drawCaption('###leftCaption', menuExchange.getLeftCaption(), leftWidth)

  menuExchange.inBorder('###inBorder_myInv', function ()
    menuExchange.drawInventory(menuExchange.myInventory)
  end)

  imgui.EndChild()
end

function menuExchange.drawRight()
  local rightWidth = (1 - menuExchange.leftWidthPercentage) * menuExchange.winWidth - menuExchange.gap * 2
  imgui.BeginChild('###exchangeMenu_right', rightWidth, menuExchange.childHeight, true)

  -- Рисуем инвентарь обмена партнера
  menuExchange.drawCaption(menuExchange.ids.rightCaption, menuExchange.getRightCaption(), rightWidth, menuExchange.partnerLock)
  menuExchange.inBorder('###inBorder_partnerExchangeInv', function ()
    menuExchange.drawInventory(menuExchange.partnerExchangeInv)
  end, nil, 0.5 * (-menuExchange.childHeight + menuExchange.gap), menuExchange.partnerLock)

  -- Рисуем наш инвентарь обмена
  local lockClicked = menuExchange.drawCaption(menuExchange.ids.rightCaption2, menuExchange.getRightCaption2(), rightWidth, menuExchange.myLock)
  if lockClicked then
    if menuExchange.myLock == 'locked' then
      -- Закомментил, потому что по ТЗ замочек нельзя открыть обратно
      --menuExchange.myLock = 'unlocked'
      --imgui.PlaySound(0x6be20)
    else
      menuExchange.myLock = 'locked'
      imgui.PlaySound(0x6be1f)
    end
  end
  menuExchange.inBorder('###inBorder_myExchangeInv', function ()
    menuExchange.drawInventory(menuExchange.myExchangeInv)
  end, nil, nil, menuExchange.myLock)

  imgui.EndChild()
end

function menuExchange.show()
  menuExchange.isVisible = true
end

function menuExchange.onRender()
  local width = imgui.GetDisplayWidth()
  local height = imgui.GetDisplayHeight()

  if menuExchange.isDialogVisible or menuExchange.isVisible then
    imgui.DisableMenu('interactionMenu', 100)
    imgui.DisableMenu('escapeMenu', 100)
  end

  if menuExchange.isDialogVisible then
    local clickedIndex = SkympDialog(menuExchange.partnerName .. ' предлагает Вам обменяться предметами', {'Да', 'Нет'})
    if clickedIndex == 1 then
      menuExchange.isDialogVisible = false
      menuExchange.show()
      sendto(menuExchange.partnerName, 'CONTENT_EXCHANGE_START_CONFIRM', menuExchange.partnerName .. ':' .. skymp.GetMyName())
    elseif clickedIndex == 2 then
      menuExchange.isDialogVisible = false
      imgui.SetCursorVisible(false)
    end
  end

  if menuExchange.isVisible then menuExchange.broadcastMyExchangeInv() end

  if menuExchange.isVisible and imgui.IsKeyDown(0x1B) then -- escape
    menuExchange.reset(); imgui.SetCursorVisible(false)
  end
  if menuExchange.isVisible == false then return end

  if menuExchange.page == 'slider' then
    local ok, err = pcall(menuExchange.drawSlider)
    if not ok then imgui.Text(err) end
  end
  --imgui.SetCursorVisible(true)

  skymp.UpdateInventoryInfo()
  if #menuExchange.myExchangeInv == 0 then
    menuExchange.recalcMyInventory(menuExchange.myInventory)
  end
  --menuExchange.recalcMyInventory(menuExchange.myExchangeInv)

  InSkympWindow('exchangeMenu', imgui.constant.WindowFlags.NoMove | imgui.constant.WindowFlags.NoResize, function ()
    imgui.SetWindowPos(0.5 * (width - menuExchange.winWidth), 0.5 * (height - menuExchange.winHeightFull))
    imgui.SetWindowSize(menuExchange.winWidth, menuExchange.winHeightFull)
    menuExchange.drawLeft()
    imgui.SameLine()
    menuExchange.drawRight()
    local txt = menuExchange.confirmButtonText
    local confirmBtnWidth = imgui.CalcTextSize(txt) * 2
    imgui.SetCursorPosX(menuExchange.winWidth - menuExchange.gap * 1 - confirmBtnWidth)
    local clicked = SkympButton('###exchangeConfirm', txt, confirmBtnWidth, menuExchange.winHeightFull - menuExchange.winHeight - menuExchange.gap)
    if menuExchange.myLock == 'unlocked' or menuExchange.partnerLock == 'unlocked' then
      ClearSkympButton('###exchangeConfirm')
    end
    if clicked and menuExchange.buttonActive then
      if menuExchange.myLock == 'unlocked' then
        imgui.PlaySound(0x6d1c6)
        SetSpriteButtonPercentage(menuExchange.ids.rightCaption2, 1)
      elseif menuExchange.partnerLock == 'unlocked' then
        imgui.PlaySound(0x6d1c6)
        SetSpriteButtonPercentage(menuExchange.ids.rightCaption, 1)
      else
        imgui.PlaySound(menuExchange.soundOk)
        menuExchange.buttonActive = false
        menuExchange.confirmButtonText = 'Ожидание ' .. menuExchange.partnerName .. ' ...'
        skymp.SendScriptEvent('exchange', menuExchange.encodeExchangeData(), function (code, body)
          --menuExchange.confirmButtonText = code .. ' ' .. body
          if code == 204 then
            menuExchange.finish()
            sendto(menuExchange.partnerName, 'CONTENT_EXCHANGE_FINISH', menuExchange.partnerName)
          end
          if code == 500 then
            menuExchange.finishError()
            sendto(menuExchange.partnerName, 'CONTENT_EXCHANGE_FINISH_ERROR', menuExchange.partnerName)
          end
        end)
      end
    end
  end)
end

function menuExchange.recalcMyInventory(t)
  local n = skymp.GetNumSlots()
  for i = 1, n do
    local slot = t[i]
    if slot == nil then slot = {}; t[i] = slot end
    slot.id = skymp.GetNthSlotId(i)
    slot.count = skymp.GetNthSlotCount(i)
    slot.name = skymp.GetNthSlotName(i)
    if slot.name == ' ' or slot.name == '' then
      slot.name = nil
    end
  end
  local tSize = #t
  if n < tSize then
    for i = n + 1, tSize do t[i] = nil end
  end
end

function menuExchange.drawSlider()
  imgui.SetNextWindowFocus()
  imgui.PushStyleColor(imgui.constant.Col.Border, -1)
  InSkympWindow('###menuSlider', imgui.constant.WindowFlags.NoMove | imgui.constant.WindowFlags.NoResize, function ()
    local w, h = 300, 135
    imgui.Text(menuExchange.sliderCaption)
    imgui.SetWindowPos(imgui.GetDisplayWidth() / 2 - w / 2, imgui.GetDisplayHeight() / 2 - h / 2)
    imgui.SetWindowSize(w, h)
    imgui.PushItemWidth(w - 18.5)
    imgui.PushStyleColor(imgui.constant.Col.SliderGrab, 0xaaffffff)
    imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, 0xbbffcc33)
    local v1, v2 = imgui.SliderInt('', menuExchange.sliderVal, menuExchange.sliderMin, menuExchange.sliderMax, "%.0f")
    imgui.PopStyleColor()
    imgui.PopStyleColor()
    imgui.PopItemWidth()
    --imgui.Text(tostring(v1)..' ' .. tostring(v2) )
    menuExchange.sliderVal = v2
    local btnW = 137
    if SkympButton('###SkympButton_MenuExchangeSloder_1', 'Отмена', btnW, 45) then
      ClearSkympButton('###SkympButton_MenuExchangeSloder_1')
      imgui.PlaySound(menuExchange.soundOk)
      menuExchange.page = 'exchange'
    end
    imgui.SameLine()
    if SkympButton('###SkympButton_MenuExchangeSloder_2', 'ОК', btnW, 45) then
      ClearSkympButton('###SkympButton_MenuExchangeSloder_2')
      imgui.PlaySound(menuExchange.soundOk)
      local f = function ()
        menuExchange.page = 'exchange'
        local slot = { id = menuExchange.sliderId, count = menuExchange.sliderVal, name = menuExchange.sliderCaption }
        menuExchange.addSlot(menuExchange.myExchangeInv, slot)
        menuExchange.removeSlot(menuExchange.myInventory, slot)
      end
      pcall(f)
    end
  end)
  imgui.PopStyleColor()
end

-- Подменяем функцию, чтобы рисовать со шрифтом
local g_onRenderOrigin = menuExchange.onRender
menuExchange.on.render = function ()
  menuExchange.myName = skymp.GetMyName()
  imgui.PushStyleColor(imgui.constant.Col.Border, 0x0033ccff)
  WithSkympFont(g_onRenderOrigin, 28)
  imgui.PopStyleColor()
end

return menuExchange
